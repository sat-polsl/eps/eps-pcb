# Hello!
Nice to have you here! Feel free to contribute, submit issues and ask questions!

# Table of contents

- [Reporitng an Issue](#reporting-an-issue)
- [Working with repsitory](#working-with-repository)
    - [Contribution flow](#contribution-flow)
    - [Commit message format](#commit-message-format)

## Reporting an issue 
Bugs are tracked with [GitLab Issue Board](https://gitlab.com/sat-polsl/eps/eps-pcb/-/boards)

Explain the problem and include additional details to help maintainers reproduce the problem.

## Working with repository
### Contribution flow

1. Branch out from `main` branch and name your branch `eps<issue_number>-<shortdescription>`.
2. Make commits to your branch.
3. Push your work to remote branch
4. Submit a Merge Request to `main` branch

### Commit message format
Use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

